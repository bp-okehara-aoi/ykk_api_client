appnope==0.1.0
astroid==2.3.3
attrs==19.3.0
backcall==0.1.0
certifi==2019.11.28
chardet==3.0.4
decorator==4.4.2
get==2019.4.13
idna==2.9
importlib-metadata==1.5.0
ipython==7.13.0
ipython-genutils==0.2.0
isort==4.3.21
jedi==0.16.0
lazy-object-proxy==1.4.3
mccabe==0.6.1
more-itertools==8.2.0
numpy==1.18.1
opencv-python==4.2.0.32
packaging==20.3
pandas==1.0.1
parso==0.6.2
pexpect==4.8.0
pickleshare==0.7.5
pluggy==0.13.1
post==2019.4.13
prompt-toolkit==3.0.3
ptyprocess==0.6.0
public==2019.4.13
py==1.8.1
Pygments==2.5.2
pylint==2.4.4
pyparsing==2.4.6
pytest==5.3.5
pytest-datadir==1.3.1
python-dateutil==2.8.1
pytz==2019.3
query-string==2019.4.13
requests==2.23.0
six==1.14.0
traitlets==4.3.3
typed-ast==1.4.1
urllib3==1.25.8
wcwidth==0.1.8
wrapt==1.11.2
zipp==3.1.0
