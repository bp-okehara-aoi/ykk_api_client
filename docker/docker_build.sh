#!/bin/bash
cd ..

IMAGE=api_client
VERSION=latest

docker build -t gpu-registry:5000/$USER/$IMAGE:$VERSION -f docker/Dockerfile .
