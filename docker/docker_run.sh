#!/bin/bash
IMAGENAME=api_client
VERSION=latest
ID=$(hostname)-${USER//[^a-zA-Z0-9]/-}
IMAGE=gpu-registry:5000/$USER/$IMAGENAME:$VERSION
MEMORY=4g # 目安は実装サイズの半分としてください gpu-1050ti-[1,2,3]=8GB, gpu-1080-1=32GB, gpu-1080ti-[1,2]=64GB

docker container run \
  --memory=$MEMORY \
  --name=$ID \
  --net=anomaly-detection_container-link \
  -v /home:/home \
  -v /home/aoi.okehara/api_client/data:/app/data \
  -v /home/aoi.okehara/api_client/result/:/app/result \
  --rm \
  $IMAGE
