import requests
import os
import json
import pandas as pd

cur_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(os.path.join(cur_dir, '..'))


def select_output_field(target_type, filename, data):
    '''
    APIから取得した予測結果jsonから、必要な項目を選択する関数
    '''
    if 'right' in target_type:
        output = [filename, 1 if data['result']["detection_models"][
            "right_edge"]['detection_result'] == 'NG' else 0]
    elif 'tape' in target_type:
        output = [filename, 1 if data['result']["detection_models"]
                  ["tape"]['detection_result'] == 'NG' else 0]
    else:
        output = [filename, 1 if data['result']
                  ['detection_result'] == 'NG' else 0]
    return output


def get_prediction_csv(light_type='bar'):
    '''
    phase2のAPIから予測結果を取得し、結果をCSVに保存する関数

    Parameters
    ----------
    light_type: str
        照明のタイプ(API側と合わせる必要があるので注意が必要)
    '''

    # 設定値のロード
    input_path = 'data'
    url = 'http://ad-api:8000/api/black/anomaly_detect'

    # 出力の生成
    output = []
    for dirpath, _, filenames in os.walk(input_path):
        # ディレクトリ名から画像領域を取得
        target_type = dirpath.split('/')[-1]
        if light_type in target_type:
            for filename in filenames:
                payload = {"img_file": f'{filename}', "save_marked_img": True}
                res = requests.post(url, json=payload)
                data = json.loads(res.text)
                output.append(select_output_field(target_type, filename, data))
            # 出力まとめ
            output_df = pd.DataFrame(output, columns=['filename', 'ph1_pred'])
            with open(f'result/{target_type}.csv', 'w') as f:
                output_df.to_csv(f, index=False)
        else:
            pass


if __name__ == "__main__":
    get_prediction_csv()
